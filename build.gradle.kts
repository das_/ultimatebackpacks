plugins {
    kotlin("jvm") version "1.3.61"
}

group = "net.seliba"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven(url = "https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven(url = "https://oss.sonatype.org/content/repositories/snapshots")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    compileOnly(group = "org.spigotmc", name = "spigot-api", version = "1.15.1-R0.1-SNAPSHOT")
}

tasks {
//    val replaceTokens = task<Copy>("replaceTokens") {
//        from("src/main/resources") {
//            include("**/plugin.yml")
//            val tokens = mapOf(
//                "releaseVersion" to (project.version as? String ?: "UnknownVersion")
//            )
//            filter<org.apache.tools.ant.filters.ReplaceTokens>(mapOf("tokens" to tokens))
//        }
//        into("build/filteredSrc")
//        includeEmptyDirs = false
//    }
//
//    val replaceTokensInSource = task<SourceTask>("replaceTokensInSource") {
//        val javaSources = sourceSets["main"].allJava.filter {
//            it.name != "plugin.yml"
//        }
//            .asFileTree
//        source = javaSources + fileTree(replaceTokens.destinationDir)
//        dependsOn(replaceTokens)
//    }
//
//    compileJava {
//        source = replaceTokensInSource.source
//        dependsOn(replaceTokensInSource)
//    }

    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}