package net.seliba.ultimatebackpacks

import net.seliba.ultimatebackpacks.configuration.Config
import net.seliba.ultimatebackpacks.configuration.ConfigurationSetting
import org.bukkit.plugin.java.JavaPlugin

object UltimateBackpacks : JavaPlugin() {
    private var config: Config? = null

    override fun onEnable() {
        this.sendStartupMessage()
        this.initiateConfig()
        this.checkForUpdates()
    }

    private fun sendStartupMessage() {
        // Send startup messages with some basic information
        this.logger.info("--------------------------------")
        this.logger.info("UltimateBackpacks successfully started!")
        this.logger.info("Author: Seliba")
        this.logger.info("Version: 1.0")
        this.logger.info("--------------------------------")
    }

    private fun initiateConfig() {
        // Create and initiate the config.yml
        this.config = Config("config.yml", this)
        // Check whether or not the configuration file exists
        if(!this.config!!.exists()) {
            // Add default comments and configuration key-value-pairs to the configuration
            this.config!!.addComment(comment = "=========================================================================================")
            this.config!!.addComment(comment = "Configuration file of UltimateBackpacks")
            this.config!!.addComment(comment = " ")
            this.config!!.addComment(comment = "If you need any support, feel free")
            this.config!!.addComment(comment = "to join my Discord server: https://discord.gg/E763gRg")
            this.config!!.addComment(comment = "===================================================================")
            TODO()
            // Save the configuration
            this.config!!.save()
        }

        // Reload the internal config storage to the new settings
        ConfigurationSetting.reloadValues()
    }

    private fun checkForUpdates() {

    }
}