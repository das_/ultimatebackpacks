package net.seliba.ultimatebackpacks.configuration

import org.bukkit.configuration.InvalidConfigurationException
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.plugin.java.JavaPlugin
import java.io.*


class Config(private val fileName: String, private val javaPlugin: JavaPlugin) : YamlConfiguration() {
    private var file: File? = null
    var existed = true

    fun addComment(comment: String, spacing: Int = 0) {
        // Create a BufferedWriter to append the comment to the current file
        val fileOutputStream = FileOutputStream(this.file!!, true)
        val outputStreamWriter = OutputStreamWriter(fileOutputStream, "UTF-8")
        val writer: Writer = BufferedWriter(outputStreamWriter)

        // Append the line as a comment with additional spacing to the file
        writer.append("${" ".repeat(spacing)}}# $comment")

        // Close all resources to prevent memory leaks
        writer.close()
        outputStreamWriter.close()
        fileOutputStream.close()
    }

    fun exists(): Boolean {
        // Return whether or not the config was generated after the last plugin restart
        return existed
    }

    private fun reload() {
        this.file = File(javaPlugin.dataFolder, name)
        try {
            if (!this.file!!.exists()) {
                this.existed = false
                if (!this.file!!.createNewFile()) {
                    throw RuntimeException("Could not create \${NAME} $name")
                }
            }
            super.load(file!!)
        } catch (e: IOException) {
            this.javaPlugin.logger.warning("Error while loading the configuration $fileName!")
            TODO()
        } catch (e: InvalidConfigurationException) {
            TODO()
        }
    }

    fun save() {
        try {
            // Save the configuration to the file
            super.save(file!!)
        } catch (e: IOException) {
            e.printStackTrace()
            TODO()
        }
    }

    override fun set(path: String, value: Any?) {
        // Put the value into the configuration file
        super.set(path, value)
        // Save the configuration file after the operation to add support for configuration comments
        this.save()
    }

    fun setDefault(path: String, value: Any?) {
        // Check if the path already has a value
        if(!super.isSet(path)) {
            // Put the value into the configuration
            this.set(path, value)
        }
    }

    init {
        // Reload the configuration after creation
        this.reload()
    }
}