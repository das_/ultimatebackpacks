package net.seliba.ultimatebackpacks.configuration

import net.seliba.ultimatebackpacks.UltimateBackpacks

enum class ConfigurationSetting(var configurationIdentifier: String, var type: Class<*>, var defaultValue: Any) {
    DEBUG_MODE("debug-mode", Boolean::class.java, false),
    PREFIX("prefix", String::class.java, "Prefix: ");

    companion object {
        private val map = HashMap<ConfigurationSetting, Any>()

        fun get(configurationSetting: ConfigurationSetting): Any {
            return map[configurationSetting]!!
        }

        fun reloadValues() {
            map.clear()
            values().forEach {
                val configValue = UltimateBackpacks.config.get(it.configurationIdentifier)!!
                if(!it.type.isInstance(configValue)) {
                    UltimateBackpacks.logger.warning("Config value ${it.configurationIdentifier} could not be read")
                    return@forEach
                }
                map[it] = configValue
            }
        }
    }
}
